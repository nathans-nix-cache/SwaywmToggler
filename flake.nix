{
  nixConfig = {
    extra-substituters =
      [ "https://cache.iog.io" "https://nix-cache.mccarty.io/" ];
    extra-trusted-public-keys =
      [ "nathan-nix-cache:R5/0GiItBM64sNgoFC/aSWuAopOAsObLcb/mwDf335A=" ];
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system};
      let
        t = lib.trivial;
        hl = haskell.lib;

        name = "swaywm-toggler";

        project = devTools:
          let addBuildTools = (t.flip hl.addBuildTools) devTools;
          in haskellPackages.developPackage {
            root = lib.sourceFilesBySuffices ./. [ ".cabal" ".hs" ];
            name = name;
            returnShellEnv = !(devTools == [ ]);
            modifier = (t.flip t.pipe) [
              addBuildTools
              hl.dontHaddock
              hl.justStaticExecutables
              hl.disableLibraryProfiling
              hl.disableExecutableProfiling
            ];
          };
      in {
        packages.swaywm-toggler = project [ ];
        defaultPackage = self.packages.${system}.swaywm-toggler;
        devShell = project (with haskellPackages; [
          hlint
          cabal-install
          haskell-language-server
          brittany
        ]);
      });
}
