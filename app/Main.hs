{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import           Control.Monad                  ( (>=>) )
import           Data.Aeson                     ( (.:)
                                                , (.=)
                                                , FromJSON(parseJSON)
                                                , ToJSON
                                                , decodeStrict
                                                , defaultOptions
                                                , genericToEncoding
                                                , object
                                                , toEncoding
                                                , withObject
                                                )
import           Data.Aeson.Types               ( ToJSON(toJSON) )
import qualified Data.ByteString               as B
import qualified Data.ByteString.Char8         as C
import           Data.Int                       ( Int32 )
import           Data.List                      ( sortBy )
import           Data.List.Extra                ( maximumOn )
import qualified Data.Serialize                as S
import qualified Data.Text                     as T
import           GHC.Generics
import           GHC.OldList                    ( sortOn )
import           Network.Socket
import           Network.Socket.ByteString
import           System.Environment             ( getEnv )
import           System.Environment.Blank       ( getArgs )
import qualified Turtle                        as X

-- Command types
data Command = RUN_COMMAND
  | GET_WORKSPACES
  | SUBSCRIBE
  | GET_OUTPUTS
  | GET_TREE
  | GET_MARKS
  | GET_BAR_CONFIG
  | GET_VERSION
  | GET_BINDING_MODES
  | GET_CONFIG
  | SEND_TICK
  | SYNC
  | GET_BINDING_STATE
  | GET_INPUTS
  | GET_SEATS

commandToInt :: Command -> Int32
commandToInt RUN_COMMAND       = 0
commandToInt GET_WORKSPACES    = 1
commandToInt SUBSCRIBE         = 2
commandToInt GET_OUTPUTS       = 3
commandToInt GET_TREE          = 4
commandToInt GET_MARKS         = 5
commandToInt GET_BAR_CONFIG    = 6
commandToInt GET_VERSION       = 7
commandToInt GET_BINDING_MODES = 8
commandToInt GET_CONFIG        = 9
commandToInt SEND_TICK         = 10
commandToInt SYNC              = 11
commandToInt GET_BINDING_STATE = 12
commandToInt GET_INPUTS        = 100
commandToInt GET_SEATS         = 101

intToCommand :: Int32 -> Maybe Command
intToCommand 0   = Just RUN_COMMAND
intToCommand 1   = Just GET_WORKSPACES
intToCommand 2   = Just SUBSCRIBE
intToCommand 3   = Just GET_OUTPUTS
intToCommand 4   = Just GET_TREE
intToCommand 5   = Just GET_MARKS
intToCommand 6   = Just GET_BAR_CONFIG
intToCommand 7   = Just GET_VERSION
intToCommand 8   = Just GET_BINDING_MODES
intToCommand 9   = Just GET_CONFIG
intToCommand 10  = Just SEND_TICK
intToCommand 11  = Just SYNC
intToCommand 12  = Just GET_BINDING_STATE
intToCommand 100 = Just GET_INPUTS
intToCommand 101 = Just GET_SEATS
intToCommand _   = Nothing

-- A mode
data Mode = Mode
  { width   :: Int
  , height  :: Int
  , refresh :: Int
  }
  deriving (Generic, Show)

instance FromJSON Mode

-- A rect
data Rect = Rect
  { x          :: Int
  , y          :: Int
  , rectWidth  :: Int
  , rectHeight :: Int
  }
  deriving Show

instance FromJSON Rect where
  parseJSON = withObject "Rect"
    $ \v -> Rect <$> v .: "x" <*> v .: "y" <*> v .: "width" <*> v .: "height"

-- The outputs
data Output = Output
  { name              :: String
  , make              :: String
  , model             :: String
  , serial            :: String
  , active            :: Bool
  , dpms              :: Bool
  , primary           :: Bool
  , scale             :: Double
  , subpixel_hinting  :: String
  , transform         :: String
  , current_workspace :: String
  , modes             :: [Mode]
  , current_mode      :: Mode
  , rect              :: Rect
  }
  deriving (Generic, Show)

instance FromJSON Output

outputToDisplay :: Output -> Display
outputToDisplay out = Display { output       = name out
                              , pos          = (x . rect $ out, y . rect $ out)
                              , displayScale = scale out
                              , res          = resolution out
                              , subpixel     = subpixel_hinting out
                              }
-- Raw display resolution
resolution :: Output -> (Int, Int)
resolution output =
  let mode = maximumOn width (modes output) in (width mode, height mode)

-- A display description
data Display = Display
  { output       :: String
  , pos          :: (Int, Int)
  , res          :: (Int, Int)
  , displayScale :: Double
  , subpixel     :: String
  }

displayToCommand :: Display -> String
displayToCommand display =
  "output "
    ++ output display
    ++ " position "
    ++ (show . fst . pos $ display)
    ++ " "
    ++ (show . snd . pos $ display)
    ++ " scale "
    ++ (show . displayScale $ display)

-- Resizes and moves the displays for scale 1.0
descale :: [Output] -> [Output]
descale outputs =
  let sortedOutputs = sortOn (x . rect) outputs in descaleHelper 0 outputs
 where
  descaleHelper :: Int -> [Output] -> [Output]
  descaleHelper _ [] = []
  descaleHelper start (x : xs) =
    let
      outputY  = (y . rect) $ x
      (w, h)   = resolution x
      newStart = start + w
      newRect  = Rect { x = start, y = outputY, rectWidth = w, rectHeight = h }
    in
      x { rect = newRect, scale = 1.0 } : descaleHelper newStart xs

-- Sends a message, and gets the response
swaymsg :: Command -> String -> Socket -> IO String
swaymsg cmd msg soc =
  let header  = C.pack "i3-ipc"
      payload = C.pack msg
      r_payload_len :: Int32
      r_payload_len = fromIntegral . B.length $ payload
      payload_len   = B.reverse . S.encode $ r_payload_len
      command_type  = B.reverse . S.encode . commandToInt $ cmd
      message       = header <> payload_len <> command_type <> C.pack msg
  in  do
        sendAll soc message
        header <- recv soc 6
        if C.unpack header == "i3-ipc" then pure () else fail "Invalid Header"
        len          <- recv soc 4
        len :: Int32 <- case S.decode . B.reverse $ len of
          Left  x -> fail $ "Invalid int " ++ x
          Right x -> pure x
        command_type          <- recv soc 4
        command_type :: Int32 <- case S.decode . B.reverse $ command_type of
          Left  x -> fail $ "Invalid int " ++ x
          Right x -> pure x
        bytestring <- recv soc (fromIntegral len)
        pure . C.unpack $ bytestring

-- Gets the outputs
getOutputs :: Socket -> IO [Output]
getOutputs x = do
  string <- swaymsg GET_OUTPUTS " " x
  case Data.Aeson.decodeStrict . C.pack $ string of
    Just x  -> pure x
    Nothing -> fail "Bad return"

-- Sends a Display command to sway
setDisplay :: Socket -> Display -> IO String
setDisplay soc display = swaymsg RUN_COMMAND (displayToCommand display) soc

main :: IO ()
main = do
  -- First find the sway socket
  sway_sock <- getEnv "SWAYSOCK"
  -- And the args
  rawargs   <- getArgs
  let (command : args) = map T.pack rawargs
  withSocketsDo $ do
    -- Connect to the socket
    soc <- socket AF_UNIX Stream 0
    connect soc (SockAddrUnix sway_sock)
    -- Get the displays
    outputs <- getOutputs soc
    -- convert to displays
    let original = map outputToDisplay outputs
    let descaled = map outputToDisplay $ descale outputs
    -- Descale the displays
    mapM_ (setDisplay soc) descaled
    -- Run our command
    X.procs command args X.empty
    -- Rescale the displays
    mapM_ (setDisplay soc) original
    -- Close up
    close soc
